from setuptools import setup

setup(
    name='nvaca_logo',
    packages=['nvaca_logo'],
    include_package_date=True,
    install_requires=[
        'flask',
        'flask-wtf',
        'flask-login',
        'esipy',
    ]
)
