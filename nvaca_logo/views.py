import os
import uuid

from werkzeug.utils import secure_filename
from flask import request, session, render_template, url_for, redirect
from esipy.exceptions import APIException

from nvaca_logo import app
from nvaca_logo import db as dbm
from nvaca_logo import esi
from nvaca_logo import forms


@app.route('/')
def index():
    details = dict()

    if 'token' in session:
        try:
            esi.esisecurity.update_token(session['token'])
        except APIException as err:
            print(err)
            return render_template('index.html', security=esi.esisecurity)
        try:
            verify = esi.esisecurity.verify()
        except:
            return render_template('index.html', security=esi.esisecurity)
        session['verify'] = verify

        try:
            session['character'] = esi.get_character(verify.get('CharacterID'))
            session['alliance'] = esi.get_alliance(session['character']['alliance_id'])
            session['corporation'] = esi.get_corporation(session['character']['corporation_id'])
        except APIException as err:
            return render_template('error.html', error=err)

    return render_template(
        'index.html',
        security=esi.esisecurity
    )


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


@app.route('/submit', methods=['GET', 'POST'])
def submit():
    form = forms.SubmitForm()

    if form.validate_on_submit():

        if not session.get('verify'):
            return "Not logged in"

        f = form.file.data
        _, extension = os.path.splitext(secure_filename(f.filename))
        filename = uuid.uuid4().hex + extension
        f.save(os.path.join(
            app.root_path, 'static', 'submissions', filename
        ))

        db = dbm.get_db()
        db.execute('INSERT INTO submissions(character_id, character_name, path, details) VALUES (?, ?, ?, ?)',
                   [session.get('verify')['CharacterID'],
                    session.get('character')['name'],
                    filename,
                    form.text.data
                    ])
        db.commit()
        return redirect(url_for('submitted'))

    return render_template('submit.html', submit_form=form)


@app.route('/submitted')
def submitted():
    db = dbm.get_db()
    cur = db.execute('SELECT id, character_id, character_name, path, details FROM submissions ORDER BY id DESC')
    submissions = cur.fetchall()
    return render_template('show_submissions.html', submissions=submissions)


@app.route('/sso/callback')
def oauth():
    code = request.args.get('code')
    token = esi.esisecurity.auth(code)
    session['token'] = token
    return redirect(url_for('index'))
