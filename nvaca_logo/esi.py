import json

from  esipy import App, EsiClient, EsiSecurity
from esipy.exceptions import APIException

from nvaca_logo import config

esiapp = App.create(config.ESI_SWAGGER_JSON)

esisecurity = EsiSecurity(
    app=esiapp,
    redirect_uri=config.ESI_CALLBACK,
    client_id=config.ESI_CLIENT_ID,
    secret_key=config.ESI_SECRET_KEY
)

esiclient = EsiClient(
    security=esisecurity,
    cache=None,
    headers={'User-Agent': config.ESI_USER_AGENT}
)


def get_character(character_id):
    """
    Get public character data.
    :param character_id: ID of character to be queried.
    :return: dict containing public character data, according to Swagger spec.
    """
    character_details = esiapp.op['get_characters_character_id'](character_id=character_id)
    result = esiclient.request(character_details)
    if result.status >= 400:
        raise APIException(character_details[0].url, result.status, json.loads(result.raw.decode('utf-8')))
    details = json.loads(result.raw.decode('utf-8'))
    return details


def get_alliance(alliance_id):
    """
    Get public alliance data.
    :param alliance_id: ID of alliance to be queried.
    :return: dict containing public alliance data, according to Swagger spec.
    """
    alliance_details = esiapp.op['get_alliances_alliance_id'](alliance_id=alliance_id)
    result = esiclient.request(alliance_details)
    if result.status >= 400:
        raise APIException(alliance_details[0].url, result.status, json.loads(result.raw.decode('utf-8')))
    details = json.loads(result.raw.decode('utf-8'))
    return details


def get_corporation(corporation_id):
    """
    Get public corporation data.
    :param corporation_id: ID of corporation to be queried.
    :return: dict containing public corporation data, according to Swagger spec.
    """
    corporation_details = esiapp.op['get_corporations_corporation_id'](corporation_id=corporation_id)
    result = esiclient.request(corporation_details)
    if result.status >= 400:
        raise APIException(corporation_details[0].url, result.status, json.loads(result.raw.decode('utf-8')))
    details = json.loads(result.raw.decode('utf-8'))
    return details
