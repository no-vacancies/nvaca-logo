from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import TextAreaField


class SubmitForm(FlaskForm):
    file = FileField('File', validators=[FileRequired(), FileAllowed(['png', 'svg', 'formats accepted'])])
    text = TextAreaField('Details')
