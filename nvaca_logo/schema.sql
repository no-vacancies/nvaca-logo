DROP TABLE IF EXISTS submissions;
CREATE TABLE submissions (
  id             INTEGER PRIMARY KEY AUTOINCREMENT,
  character_id   INTEGER NOT NULL,
  character_name TEXT    NOT NULL,
  'path'         TEXT    NOT NULL,
  details        TEXT    NOT NULL
);