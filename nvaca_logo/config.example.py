# -*- encoding: utf-8 -*-
import datetime

# ----------------------------------------
# Application config
# ----------------------------------------
HOST = '0.0.0.0'
PORT = 5000
DEBUG = True
SECRET_KEY = 'secret'

# ----------------------------------------
# ESI config
# ----------------------------------------
ESI_DATASOURCE = 'tranquility'
ESI_SWAGGER_JSON = 'https://esi.tech.ccp.is/latest/swagger.json?datasource={}'.format(ESI_DATASOURCE)
ESI_CLIENT_ID = 'clientid'
ESI_SECRET_KEY = 'secretkey'
ESI_CALLBACK = 'http://localhost:{}/sso/callback'.format(PORT)
ESI_USER_AGENT = 'nvaca-logo-example'

# ----------------------------------------
# Alliance config
# ----------------------------------------

AUTHORIZED_ALLIANCE_ID = 'allianceid'

# ----------------------------------------
# Session config
# ----------------------------------------
PERMANENT_SESSION_LIFETIME = datetime.timedelta(days=30)

SQLALCHEMY_TRACK_MODIFICATIONS = True
