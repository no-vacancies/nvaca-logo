#!/usr/bin/env python
import os
import uuid

from esipy.exceptions import APIException
from flask import Flask, request, session, render_template, url_for, redirect
from werkzeug.utils import secure_filename

from nvaca_logo import config

app = Flask(__name__)
app.config.from_object(config)

import nvaca_logo.views
