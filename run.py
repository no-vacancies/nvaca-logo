from nvaca_logo import app, config

app.run(debug=config.DEBUG, host=config.HOST, port=config.PORT)
